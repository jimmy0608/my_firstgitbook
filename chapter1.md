# 第一章

## 使用GitBook來寫本書吧!

如果用 Markdown 能輕鬆撰寫一本電子書的話，那該有多好啊！那就來試試看GitBook吧!

- [GitBook官網](https://www.gitbook.com/) - 註冊一下，開始寫你的第一本GitBook
- [GitBook介面介紹](https://www.youtube.com/watch?v=wJhHJJkTQS4&index=3&list=PLDdZo0QJkJD8CxzUjAc_vTPQGP_ND_fj1)

大致上的用法就跟一般Markdown相差無幾，但是要注意裡面的README.md與SUMMARY.md兩個檔案，這裡的README.md是這本Gitbook的簡介，SUMMARY.md是目錄，如果你觀察力夠敏銳，你其實早就發現這份教材其實也是用GitBook製作的。





## #第一部分：申請Gitlab、安裝Gitkraken、安裝Typora、安裝Atom



1. 申請Gitlab 

​		可以先了解一下[什麼是Git](https://https//blog.techbridge.cc/2018/01/17/learning-programming-and-coding-with-python-git-and-github-tutorial/)。而Github或Gitlab，都是不錯代碼托管的大平台，而這兩家		中，雖然Github一直都是龍頭，但Gitlab提供的page服務、以及gitbook template，讓我		們可以快速地佈署我們的Gitbook，因此我們要先去申請一個[Gitlab帳號](https://gitlab.com/users/sign_up)



2. 安裝Gitkraken
3. 安裝Typora
4. 試著練習編輯一個Martdown